var mcLayer = {
  queues: [],
  Init  : function (o) {
    if (o) {
      console.log('开始初始化一个实例');
      console.log('↓');
      console.log('创建一个容器DOM');
      var cDiv       = document.createElement('div'), sub = mcLayer.queues.length;
      cDiv.innerHTML = '<div id="' + (o.id || 'mc-layer-' + (sub + 1)) + '" class="mc-layer" theme="' + (o.theme || 'aa') + '"><div class="mc-layer-wrap"><div class="mc-layer-load"></div></div></div>';
      console.log('↓');
      console.log('将DOM容器装入实例');
      console.log('↓');
      this.item = cDiv.children[0];
      console.log('将配置装入实例并设置默认配置');
      console.log('↓');
      this.code = o.code || '';
      console.log('code->' + this.code);
      console.log('↓');
      this.url = o.url || '';
      console.log('url->' + this.url);
      console.log('↓');
      this.fix = o.fix || 'body';
      console.log('fix->' + this.fix);
      console.log('↓');
      this.ins = o.ins || document.body;
      console.log('ins->' + this.ins);
      console.log('↓');
      this.wrap = this.item.children[0];
      console.log('wrap->' + this.wrap);
      console.log('↓');
      this.err = o.err || '载入失败!';
      console.log('err->' + this.err);
      console.log('↓');
      this.sub = sub;
      console.log('sub->' + this.sub);
      console.log('↓');
      console.log('把实例存入队列');
      console.log('↓');
      mcLayer.queues.push(this);
      console.log(mcLayer.queues);
    }
  }
};

var amEnd = (function () {
  var evs = {
    WebkitAnimation: 'webkitAnimationEnd',
    animation      : 'animationend'
  };
  for (var k in evs) {
    if (typeof evs[k] === 'string') {
      return evs[k];
    }
  }
})();

mcLayer.Init.prototype.show = function (o) {
  console.log('0.截入初始化信息');
  console.log('↓');
  o.init = this;
  console.log('1.载入主体内容处理器');
  console.log('↓');
  o.next = function (o) {
    console.log('载入主体内容到DOM容器');
    console.log('↓');
    o.init.wrap.innerHTML = '';
    o.init.wrap.appendChild(o.init.main);

    setTimeout(function () {
      console.log('显示主体内容');
      console.log('↓');
      o.init.item.setAttribute('view', 'show');
      console.log('执行实例成功后的回调');
      console.log('↓');
      o && o.then && o.then(o);
    }, 100);
  };
  console.log('2.插入弹层容器');
  console.log('↓');
  o.init.ins.appendChild(o.init.item);
  console.log('3.创建主体DOM');
  console.log('↓');
  var cDiv    = document.createElement('div'), loadDom = '';
  o.init.main = '<div>' + o.init.err + '</div>';
  console.log('4.准备主体内容');
  console.log('↓');
  if (o.init.code) {
    console.log('5.如果配置有DOM则载入');
    console.log('↓');
    cDiv.innerHTML = o.init.code;
    o.init.main    = cDiv.children[0];
    console.log(o.init.main);
    console.log('6.主体内容载入成功则执行主体成功后续');
    console.log('↓');
    mainSuccess();
  } else if (o.init.url) {
    console.log('5.如果配置有URL则加载文件载入');
    console.log('↓');
    var load = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    load.open('GET', this.url, true);
    load.onreadystatechange = function () {
      if (this.readyState === 4) {
        if (this.status >= 200 && this.status < 400) {
          console.log('6.如果文件载加成功则载入到主体');
          console.log('↓');
          loadDom = this.response;
        } else {
          console.log('6.如何文件加载失败则执行主体错误后续');
          console.log('↓');
          mainError();
        }
        var loadTimer = setInterval(function () {
          if (loadDom) {
            clearInterval(loadTimer);
            cDiv.innerHTML = loadDom;
            o.init.main    = cDiv.children[0];
            console.log(o.init.main);
            console.log('7.主体内容载入成功则执行主体成功后续');
            console.log('↓');
            mainSuccess();
          }
        }, 100);
      }
    };
    load.send();
    load = null;
  } else {
    console.log('4.如果没有配置DOM也没有配置URL则载入错误信息');
    console.log('↓');
    console.log(o.init.main);
    mainError();
  }

  function mainSuccess() {
    console.log('预处理主体内容');
    console.log('↓');
    o && o.prep ? o.prep(o) : o.next();
  }

  function mainError() {
    console.log('载入失败退出实例');
    console.log('↓');
    o.init.item.remove();
    console.log('从队列中清除该条记录');
    console.log('↓');
    mcLayer.queues.splice(o.init.sub, 1);
    console.log(mcLayer.queues);
    console.log('执行本次实例失败后的回调');
    console.log('↓');
    o && o.error && o.error();
  }

};
mcLayer.Init.prototype.hide = function (o) {
  o.init = this;
  o.next = function (o) {
    console.log(o);
    o.init.item.setAttribute('view', 'hide');
    o.init.item.addEventListener(amEnd, function () {
      o.init.item.remove();
    });
    o && o.then && o.then(o);
  };
  o && o.prep ? o.prep(o) : o.next();
};

window.fekit ? window.fekit.mcLayer = mcLayer : window.fekit = {mcLayer: mcLayer};
module.exports = mcLayer;
