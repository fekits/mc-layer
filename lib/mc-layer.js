/**
* key                            String               *     弹框维一标识符
* passCode                       String                     密码                
* content                        React.component      *     内容
* dark                           Number                     暗层 0-1
* pos                            String                     弹层位置【左上-lt，中上-ct，右上-rt，左中-lm，中中-cm，右中-rm，左下-lb，中下-cb，右下-rb】 默为cm
                                                            lt ------ ct ------ rt
                                                            |                    |
                                                            |                    |
                                                            |                    |
                                                            lm ------ cm ------ rm
                                                            |       默认为cm      |
                                                            |                    |
                                                            |                    |
                                                            lb ------ cb ------ rb
* autoHide                       Boolen                     自动隐藏
* darkClickClose                 Boolen                     是否点击暗层关闭图层
* mode                           String                     模式【覆盖-cover，排队-queue，叠加-stack】
* animate                        String                     动画配置，需引入相应的动画主题SCSS
* on                             Object                     回调事件集合
* on.show                        Function                   显示时执行与cb一样
* on.hide                        Function                   隐藏时执行
* cb                             Function                   显示或隐藏后执行
*/

import _ajax from 'mc-ajax';

// 转换为DOM
const _html = function (str) {
  let div = document.createElement('div');
  if (typeof str == 'string')
    div.innerHTML = str;
  return div.children[0];
};

let layer = {
  tasks: {},
  error: function () { },
  show: function (param = {}) {
    let _id = 'MC_LAYER_' + Object.keys(layer.tasks).length;
    let { id = _id, area = document.body, url = '', code = '', dark = 1, pos = 'cm', autoHide = 0, darkClickClose = 0, mode = 'stack', animate = 'aa', on: { show = () => { }, hide = () => { }, fail = () => { } } = {} } = param;
    let dom = null;
    if (url) {
      _ajax({
        url: url,
        type: 'GET',
        dataType: 'text',
        success(res) {
          dom = _html(res);
        }
      });
    } else if (code) {
      dom = _html(code);
    }
    let task = { id, area, code, dom, dark, pos, autoHide, darkClickClose, mode, animate, on: { show, hide, fail } };
    console.log(task);
    layer.tasks[id] = task;
  },
  hide: function (param) {
    console.log(param);
  }
};

export default layer;